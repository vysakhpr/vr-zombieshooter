﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class EnemyHealth : MonoBehaviour
{
	public int healthPoints;
	public int scorePoints;
	public bool isDead = false;
	Animator anim;
	PlayerHealth playerHealth;

	// Use this for initialization
	void Start ()
	{
		scorePoints = healthPoints;
		anim = GetComponent<Animator> ();
		playerHealth = GameObject.FindGameObjectWithTag ("Player").GetComponent <PlayerHealth> ();
	}
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void TakeDamage (int h)
	{
		
		if (!isDead) {
			healthPoints = healthPoints - h;
            anim.SetTrigger("IsHurt");
			if (healthPoints <= 0) {
				isDead = true;
				anim.SetTrigger ("IsDead");
				playerHealth.SetCountText (scorePoints);
				DestroyImmediate (GetComponent<CapsuleCollider> ());
				DestroyImmediate (GetComponent<SphereCollider> ());
                DestroyImmediate(GetComponent<AudioSource>());
                DestroyImmediate(GetComponent<Rigidbody>());
                DestroyImmediate(GetComponent<CardboardAudioSource>());
				UnityEngine.AI.NavMeshAgent nav = GetComponent<UnityEngine.AI.NavMeshAgent> ();
				nav.speed = 0;
			}
		}
	}

}
