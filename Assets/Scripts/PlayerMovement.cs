using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
	public float speed;
	public float fallDownParam;
	private Rigidbody rb;
	private Rigidbody rbCam;
	private int count;
	Vector3 movement;
    private AndroidJavaObject plugin;
    private PlayerHealth playerHealth;

    bool isDead;

    //For walk
    private float[] gravity = new float[3];
    private float[] smoothed = new float[3];

    private double bearing = 0;

    private int stepCount;
    private double prevY;
    private double walkThreshold = 0.45;
    private double runThreshold = 0.58;
    private bool ignore = false;
    private int countdown = 6;
    ControllerInterface ci;

    int walkCount;
    int runCount;

    DateTime time;

    void Awake ()
	{
		isDead = false;
		rb = GetComponent<Rigidbody> ();
		rbCam = GameObject.FindGameObjectWithTag ("Player").GetComponent<Rigidbody> ();
		rb.constraints = (RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation);

        plugin = new AndroidJavaClass("jp.kshoji.unity.sensor.UnitySensorPlugin").CallStatic<AndroidJavaObject>("getInstance");
        plugin.Call("setSamplingPeriod", 1000); // refresh sensor 100 mSec each
        plugin.Call("startSensorListening", "accelerometer");

        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        walkCount = 0;
        runCount = 0;
        ci = GameObject.FindGameObjectWithTag("PlayerBody").GetComponent<ControllerInterface>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="pick up")
        {
            playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
            playerHealth.CaptureChest();
            other.gameObject.GetComponent<AudioSource>().Play();
            other.gameObject.transform.Translate(0, -10000, 0, Space.World);   
        }
    }

	void Update ()
	{
        if (ci.gameStart)
        {
            if (runCount > 0)
            {
                movement = 12 * Camera.main.transform.forward * Time.deltaTime;
                rbCam.MovePosition(transform.position + movement);
                runCount--;
                walkCount = 0;
            }
            else if (walkCount > 0)
            {
                movement = 1 * Camera.main.transform.forward * Time.deltaTime;
                rbCam.MovePosition(transform.position + movement);
                walkCount--;
            }

            if (time.AddSeconds(0.1) < DateTime.Now)
            {
                walkCount = 0;
                runCount = 0;
            }


            if (plugin != null)
            {

                float[] sensorValue = plugin.Call<float[]>("getSensorValues", "accelerometer");

                smoothed = lowPassFilter(sensorValue, gravity);
                gravity[0] = smoothed[0];
                gravity[1] = smoothed[1];
                gravity[2] = smoothed[2];

                Debug.Log(gravity[0] + " " + gravity[1] + " " + gravity[2]);

                if (ignore)
                {
                    countdown--;
                    ignore = (countdown < 0) ? false : ignore;
                }
                else
                    countdown = 6;

                if ((Math.Abs(prevY - gravity[1]) > runThreshold && !ignore))
                {
                    runCount += 20;
                    time = DateTime.Now;
                    ignore = true;
                }
                else if ((Math.Abs(prevY - gravity[1]) > walkThreshold && !ignore))
                {
                    walkCount += 10;
                    time = DateTime.Now;
                    ignore = true;
                }
                prevY = gravity[1];
            }
        }
    }

    protected float[] lowPassFilter(float[] input, float[] output)
    {
        if (output == null) return input;
        for (int i = 0; i < input.GetLength(0); i++)
        {
            output[i] = output[i] + 1.0f * (input[i] - output[i]);
        }
        return output;
    }

    public void PlayerDead ()
	{
		isDead = true;
		rb.mass = fallDownParam * 100;
		rb.rotation = Quaternion.Euler (new Vector3 (fallDownParam / 10, 0f, 0f));
		rb.constraints = RigidbodyConstraints.None;
	}

}
