﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterManager : MonoBehaviour {

	public GameObject enemy;
	public float spawnTime;
	public float initialSpawnTime=15f;
	public Transform[] spawnPoints;

	// Use this for initialization
	void Start () 
	{
        //TriggerMonsters();
	}

    public void TriggerMonsters()
    {
        InvokeRepeating("Spawn", initialSpawnTime, spawnTime);
    }

	void Spawn()
	{
		int spawnPointIndex = Random.Range (0, spawnPoints.Length);
		Instantiate (enemy, spawnPoints [spawnPointIndex].position, spawnPoints [spawnPointIndex].rotation);
	}

}
