﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class PlayerHealth : MonoBehaviour
{
	public int startingHealth;
	public int curHealth;
	public Slider healthSlider;
	public Image damageImage;
	public AudioClip deathClip;
	public float flashSpeed = 1f;
	public Color flashColor = new Color (1f, 0f, 0f, 0.3f);

	int count;
	int score;
    int chestCount,totalChestCount;
	public Text gameText;
	public Text scoreText;

	PlayerMovement playerBody;
	GunMovement gunMovement;

	Animator anim;
	AudioSource playerAudio;

	PlayerShoot playerShoot;
	bool isDead;
	bool damaged;
	// Use this for initialization
	void Awake ()
	{
		anim = GetComponent <Animator> ();
		//playerAudio = GetComponent <AudioSource> ();
		gunMovement = GameObject.FindGameObjectWithTag ("Gun").GetComponent <GunMovement> ();
		playerShoot = GetComponentInChildren <PlayerShoot> ();
		playerBody = GameObject.FindGameObjectWithTag ("PlayerBody").GetComponent<PlayerMovement> ();
		curHealth = startingHealth;
		gameText.text = "";
		score = 0;
		count = -1;
        chestCount = -1;
        totalChestCount = 6;
		SetCountText (0);
        CaptureChest();
	}

	void Update ()
	{
		if (damaged || isDead) {
			damageImage.color = flashColor;
		} else {
			damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
		}
		damaged = false;
	}

	public void TakeDamage (int amount)
	{
		damaged = true;

		curHealth -= amount;

		healthSlider.value = curHealth;

		//playerAudio.Play ();

		if (curHealth <= 0 && !isDead) {
			Death ();
		}
	}

	void Death ()
	{
		isDead = true;
		flashColor = new Color (1f, 0f, 0f, 0.2f);
		GetComponent<CapsuleCollider> ().enabled = false;
		playerShoot.Kill ();	
		gameText.text = "GAME OVER!";
        scoreText.text = "Shake your controller to restart";
		gunMovement.Gunfall ();
		playerBody.PlayerDead ();
        //playerAudio.clip = deathClip;
        //playerAudio.Play ();


        //playerShooting.enabled = false;
       
    }

    void Win()
    {
        isDead = true;
        flashColor = new Color(0f, 1f, 0f, 0.2f);
        GetComponent<CapsuleCollider>().enabled = false;
        playerShoot.Kill();
        gameText.text = "YOU WON!";
        scoreText.text = "Shake your controller to Restart";
        gunMovement.Gunfall();
        playerBody.PlayerDead();
    }

	public void SetCountText (int point)
	{
		count += 1;
		score += point;
		scoreText.text = "Zombie :" + count.ToString () + "\nPoints :" + score.ToString ()+"\nChests :" + chestCount.ToString()+"/"+totalChestCount.ToString();
	}

    public void CaptureChest()
    {
        chestCount += 1;
        scoreText.text = "Zombie :" + count.ToString() + "\nPoints :" + score.ToString() + "\nChests :" + chestCount.ToString() + "/" + totalChestCount.ToString();
        if(chestCount==7)
        {
            Win();
        }
    }
}
