﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{

	public float timeBetweenAttacks = 0.5f;
	public int attackDamage = 10;


	Animator anim;
	GameObject player;
	PlayerHealth playerHealth;
	EnemyHealth enemyHealth;
	public bool playerInRange;
	float timer;


	void Awake ()
	{
		player = GameObject.FindGameObjectWithTag ("Player");

		playerHealth = player.GetComponent <PlayerHealth> ();
		enemyHealth = GetComponent<EnemyHealth> ();
		anim = GetComponent <Animator> ();
		anim.SetBool ("IsWalk", true);
		//anim.SetBool ("IsAttack", false);
	}


	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject == player) {
			playerInRange = true;
            anim.SetBool("IsWalk", false);
            anim.SetBool("IsAttack", true);
		}
	}


	void OnTriggerExit (Collider other)
	{
		if (other.gameObject == player) {
			playerInRange = false;
            anim.SetBool("IsWalk", true);
            anim.SetBool("IsAttack", false);
        }
	}


	void Update ()
	{
		timer += Time.deltaTime;
		if (timer >= timeBetweenAttacks && playerInRange && enemyHealth.healthPoints > 0) {
			Attack ();
		}

		if (playerHealth.curHealth <= 0) {
            //anim.SetTrigger ("PlayerDead");
            anim.SetBool("IsAttack", false);
        }
	}


	void Attack ()
	{
		timer = 0f;
		if (playerHealth.curHealth > 0) {
			playerHealth.TakeDamage (attackDamage);
		}
			
	}
}