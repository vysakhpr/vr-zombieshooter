﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunMovement : MonoBehaviour
{

	public Vector3 positionOffSet;
	public Vector3 rotationOffSet;
	public Vector3 playerAngles;
	public Vector3 playerBodyAngles;
	public Vector3 gunAngles;
	GameObject gun,primaryGun;
	public GameObject playerBody;
	Rigidbody rb;

	public Camera player;
	// Use this for initialization
	void Start ()
	{
		positionOffSet.Set (0.126f, -0.49f, 0.408f);
		rotationOffSet.Set (0f, 95.5130f, 0f);
		rb = GetComponent<Rigidbody> ();
		//rotationOffSet.Set (0f,0f,0f);
		//player=GameObject.FindGameObjectWithTag("Player");
		gun = GameObject.FindGameObjectWithTag ("Gun");
        primaryGun = GameObject.FindGameObjectWithTag("PrimaryGun");
		gun.GetComponent<Rigidbody> ().isKinematic = true;
        primaryGun.GetComponent<Rigidbody>().isKinematic = true;


    }

    // Update is called once per frame
    void Update ()
	{
		playerAngles = player.transform.eulerAngles;
		//playerBody.transform.rotation (0f, playerAngles.y, 0f);
		//gunAngles = gun.transform.eulerAngles;

	}

	public void Gunfall ()
	{
		gun.GetComponent<BoxCollider> ().enabled = true;
        primaryGun.GetComponent<BoxCollider>().enabled = true;
        //GetComponent<BoxCollider> ().enabled = true;
        //rb.isKinematic = false;
        gun.GetComponent<Rigidbody> ().isKinematic = false;
        primaryGun.GetComponent<Rigidbody>().isKinematic = false;
        //rb.constraints = RigidbodyConstraints.None;
        //RigidbodyConstraints.FreezePosition = false;
        //rb.constraints = RigidbodyConstraints.FreezeRotation = false;
    }
}
