﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{

    public int damagePerShot = 1, primaryDamagePerShot = 1,ammo,catridge,maxCatridge;
	public float timeBetweenBullets = 0.15f,primaryTimeBetweenBullets=0.5f;
	public float range = 100f,primaryRange=50f;
    public bool toShoot = false, weaponChange = false,restartCount;
    public float scale;
    public bool primaryWeaponActive;

	float timer;
	bool isDead;
	Ray shootRay;
	RaycastHit shootHit;
	int shootableMask;
	ParticleSystem gunSparks,primaryGunSparks;
	AudioSource gunAudio,primaryGunAudio;
	public Camera player;
    GameObject sphere,gun,gunTip,primaryGun,primaryGunTip;
    GameObject laser,primaryLaser;

	// Use this for initialization
	/// <summary>
    /// 
    /// </summary>
    void Awake ()
	{
		isDead = false;
		shootableMask = LayerMask.GetMask ("Shootable");
		gunSparks = GameObject.FindGameObjectWithTag ("GunSpark").GetComponent <ParticleSystem> ();
        primaryGunSparks = GameObject.FindGameObjectWithTag("PrimaryGunSpark").GetComponent<ParticleSystem>();
        gunAudio = GetComponent<AudioSource> ();
        //sphere = GameObject.FindGameObjectWithTag("Laser");
        gun = GameObject.FindGameObjectWithTag("Gun");
        gunTip = GameObject.FindGameObjectWithTag("GunShoot");
        laser = GameObject.FindGameObjectWithTag("Laser");
        primaryGun = GameObject.FindGameObjectWithTag("PrimaryGun");
        primaryGunTip = GameObject.FindGameObjectWithTag("PrimaryGunSpark");
        primaryLaser = GameObject.FindGameObjectWithTag("PrimaryLaser");
        primaryGunAudio = primaryGun.GetComponent<AudioSource>();
        primaryWeaponActive = true;
        //primaryGun.transform.Translate(0f, -10f, 0.63f, Space.Self);
        gun.transform.Translate(0f, -1000f, 0f,Space.Self);
        ammo = 150;
        maxCatridge = 30;
        catridge = 30;
        restartCount = false;
        //ChangeGun();
        //ChangeGun();


    }

    public void Reload()
    {
        if (isDead)
        {
            if (restartCount)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
            else
            {
                restartCount = true;
            }
        }
        else if (!primaryWeaponActive)
        {
            if (ammo != 0)
            {
                int diff = maxCatridge - catridge;
                ammo = ammo - diff;
                if (ammo <= 0)
                {
                    catridge = maxCatridge + ammo;
                    ammo = 0;

                }
                else
                {
                    catridge = maxCatridge;
                }
            }
        }
    }

    public void ChangeGun()
    {
        primaryWeaponActive = !primaryWeaponActive;
        if (primaryWeaponActive)
        {
            //primaryGun.transform.Rotate(-90f, 0f, 0f,Space.World);
            primaryGun.transform.Translate(0f, 0f, 1000f);
            //gun.transform.Rotate(90f, 0f, 0f, Space.World);
            gun.transform.Translate(0f, -1000f, 0f);

        }
        else
        {
            //gun.transform.Rotate(-90f, 0f, 0f,Space.World);
            primaryGun.transform.Translate(0f, 0f, -1000f);

            gun.transform.Translate(0f, 1000f, 0f);

        }
    }


    public void TriggerShoot()
    {
        toShoot = true;
    }
	
    public void TriggerChangeWeapon()
    {
        weaponChange = true;
    }
	// Update is called once per frame
	void Update ()
	{
        if(weaponChange)
        {
            ChangeGun();
            weaponChange = false;
        }
		timer += Time.deltaTime;
        if (primaryWeaponActive)
        {
            if (timer >= primaryTimeBetweenBullets && !isDead)
            {
                primaryGunSparks.Stop();
                if (toShoot)
                {
                    Shoot();
                    toShoot = false;
                }
            }
        }
        else
        {
            if (timer >= timeBetweenBullets && !isDead)
            {
                gunSparks.Stop();
                if (toShoot)
                {
                    
                    if (catridge != 0)
                    {
                        catridge = catridge - 1;
                        Shoot();
                        toShoot = false;
                    }
                }
            }
        }
	}

    

    public void Kill ()
	{
		isDead = true;
		gunSparks.Stop ();
	}

	void Shoot ()
	{
		timer = 0f;
        if (primaryWeaponActive)
        {
            primaryGunAudio.Play();
            primaryGunSparks.Stop();
            primaryGunSparks.Play();
            shootRay.origin = primaryLaser.transform.position;
            shootRay.direction = primaryLaser.transform.forward;

            if (Physics.Raycast(shootRay, out shootHit, primaryRange, shootableMask))
            {
                EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();
                if (enemyHealth != null)
                {
                    enemyHealth.TakeDamage(primaryDamagePerShot);
                }
            }
        }
        else
        {
            gunAudio.Play();
            gunSparks.Stop();
            gunSparks.Play();
            shootRay.origin = laser.transform.position;
            shootRay.direction = laser.transform.forward;

            if (Physics.Raycast(shootRay, out shootHit, range, shootableMask))
            {
                EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();
                if (enemyHealth != null)
                {
                    enemyHealth.TakeDamage(damagePerShot);
                }
            }
        }

		
	}

}
