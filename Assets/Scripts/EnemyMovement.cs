﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

	Transform player;
	UnityEngine.AI.NavMeshAgent nav;
    EnemyHealth eh;
    Animator anim;
    Transform enemy;
    public float cryDelay;
    CardboardAudioSource cas;
    AudioSource ausource;
    float timer,decay_timer;
	void Awake()
	{
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		nav = GetComponent <UnityEngine.AI.NavMeshAgent> ();
        eh = GetComponent<EnemyHealth>();
        anim = GetComponent<Animator>();
        enemy = GetComponent<Transform>();
        //cas = GetComponent<CardboardAudioSource>();
        ausource = GetComponent<AudioSource>();
        timer = 0f;
        decay_timer = 0f;
	}

	void Update()
	{
        timer += Time.deltaTime;
        if(eh.isDead)
        {
            decay_timer += Time.deltaTime;
            if (decay_timer >10)
            {
                enemy.Translate(0, -10000, 0, Space.World);
            }
        }
        if (timer >cryDelay && !(eh.isDead))
        {
            timer = 0;
            //cas.Play();
            ausource.Play();
        }
        if (anim.GetBool("IsWalk"))
        {
            nav.SetDestination(player.position);
        }
        else
        {
            nav.SetDestination(enemy.position);
        }
	}
}
