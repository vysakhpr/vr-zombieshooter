﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestManager : MonoBehaviour {

    // Use this for initialization
    public GameObject chest;
    public Transform[] spawnPoints;

    // Use this for initialization
    void Start()
    {
        Spawn();
    }

    void Spawn()
    {
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);
        for (spawnPointIndex = 0; spawnPointIndex < spawnPoints.Length; spawnPointIndex++)
        {
            spawnPoints[spawnPointIndex].Translate(0f, 1.5f, 0, Space.World);
            Instantiate(chest, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
        }
    }
}
