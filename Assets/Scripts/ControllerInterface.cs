﻿using Matrix.Xmpp.Client;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerInterface : MonoBehaviour {

    PlayerShoot playerShoot;
    Rigidbody rbCam;
    Vector3 movement;
    bool isWalking;
    bool isBackMove = false;
    public bool gameStart = false;
    Text gt, st;
    MonsterManager[] mm;
    PlayerHealth ph;
    
    // Use this for initialization
    void Awake () {
        rbCam = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>();
        playerShoot =(GameObject.FindGameObjectWithTag("GunShoot")).GetComponent<PlayerShoot>();
        string lic = "";

		Matrix.License.LicenseManager.SetLicense(lic);


		var xmppClient = new XmppClient();
		xmppClient.SetUsername("nilesh");
		xmppClient.Port = 5222;
		xmppClient.SetXmppDomain("192.168.43.238");
		xmppClient.Password = "nilesh";
		xmppClient.OnError += XmppClient_OnError;

		xmppClient.Show = Matrix.Xmpp.Show.Chat;

		xmppClient.OnLogin += XmppClient_OnLogin;
		xmppClient.OnMessage += XmppClient_OnMessage;
		xmppClient.OnAuthError += XmppClient_OnAuthError;
		xmppClient.OnBeforeSasl += XmppClient_OnBeforeSasl;
		xmppClient.OnBeforeSendPresence += XmppClient_OnBeforeSendPresence;
		xmppClient.OnPresence += XmppClient_OnPresence;
		xmppClient.Open();

		Debug.Log("Open Called");

        gt = GameObject.FindGameObjectWithTag("GameText").GetComponent<Text>();
        st = GameObject.FindGameObjectWithTag("ScoreText").GetComponent<Text>();
        mm = GameObject.FindGameObjectWithTag("MonsterManager").GetComponents<MonsterManager>();
        ph = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        gt.text = "Zombie Shooter";
        st.text = "Shoot once to start the game";
        //StartGame();
    }

	private void XmppClient_OnPresence(object sender, PresenceEventArgs e)
	{
		Debug.Log(" On presence " + e.Presence.Status);
	}

	private void XmppClient_OnBeforeSendPresence(object sender, PresenceEventArgs e)
	{
		Debug.Log("before presence" + e.Presence.Status);
	}

	private void XmppClient_OnBeforeSasl(object sender, Matrix.Xmpp.Sasl.SaslEventArgs e)
	{
		Debug.Log("Before sasl");
	}

	private void XmppClient_OnAuthError(object sender, Matrix.Xmpp.Sasl.SaslEventArgs e)
	{
		Debug.Log("Auth Error " + e.Exception.Message);
	}

	private void XmppClient_OnMessage(object sender, MessageEventArgs e)
	{
        if (gameStart)
        {
            //Debug.Log("On Message " + e.Message);
            //toShoot = true;
            Debug.Log(e.Message.Body);
            if (e.Message.Body == "sh")
            {
                playerShoot.TriggerShoot();
            }
            else if (e.Message.Body == "cw")
            {
                playerShoot.TriggerChangeWeapon();
            }
            else if (e.Message.Body == "rl")
            {
                playerShoot.Reload();
            }
            else if (e.Message.Body == "start")
            {
                isWalking = true;
            }
            else if (e.Message.Body == "stop")
            {
                isWalking = false;
            }
            else if(e.Message.Body == "backmove")
            {
                isBackMove = true;
            } else if(e.Message.Body == "backstop")
            {
                isBackMove = false;
            }
        }
        else
        {
            if (e.Message.Body == "sh")
            {
                StartGame();
            }
        }
	}

    public void StartGame()
    {
        gameStart = true;
        gt.text = "";
        ph.SetCountText(0);
        for (int i=0;i<mm.Length;i++)
        {
            mm[i].TriggerMonsters();
        }
    }

	private void XmppClient_OnError(object sender, Matrix.ExceptionEventArgs e)
	{
		Debug.Log("On Error " + e.Exception.StackTrace);
	}

	private void XmppClient_OnLogin(object sender, Matrix.EventArgs e)
	{
		Debug.Log("auth success");
	}

	
	// Update is called once per frame
	void Update () {
        if (isWalking)
        {
            movement = 7 * Camera.main.transform.forward * Time.deltaTime;
            rbCam.MovePosition(rbCam.transform.position + movement);
        }
        if(isBackMove)
        {
            movement = -2 * Camera.main.transform.forward * Time.deltaTime;
            rbCam.MovePosition(rbCam.transform.position + movement);
        }
    }
}
